<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\NotificationMessageInterface;
use Interactiv4\Contracts\SPL\Notification\Api\NotificationMessageTrait;

/**
 * Class NotificationMessage.
 *
 * Representation of an outgoing, client-side notification request.
 * Use this class directly or as base class for other implementations.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification
 */
class NotificationMessage implements NotificationMessageInterface
{
    use NotificationMessageTrait;

    /**
     * NotificationMessage constructor.
     *
     * @param int         $notificationLevel
     * @param string|null $notificationTitle
     * @param string|null $notificationBody
     * @param array       $notificationAdditionalData
     */
    public function __construct(
        int $notificationLevel,
        ?string $notificationTitle = null,
        ?string $notificationBody = null,
        array $notificationAdditionalData = []
    ) {
        $this->notificationLevel = $notificationLevel;
        $this->notificationTitle = $notificationTitle;
        $this->notificationBody = $notificationBody;
        $this->notificationAdditionalData = $notificationAdditionalData;
    }
}
