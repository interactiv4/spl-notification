<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\NotificationChannelInterface;
use Interactiv4\Contracts\SPL\Notification\Api\NotificationChannelTrait;
use Interactiv4\Contracts\SPL\Notification\Api\NotificationLevelInterface;

/**
 * Class NotificationChannel
 *
 * Handle outgoing requests for notification messages to an specific endpoint.
 * Use this class as base class for other implementations.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification
 */
abstract class NotificationChannel implements NotificationChannelInterface
{
    use NotificationChannelTrait;

    /**
     * NotificationChannel constructor.
     *
     * @param NotificationLevelInterface $notificationLevel
     * @param int                        $notificationLevelMask
     */
    public function __construct(
        NotificationLevelInterface $notificationLevel,
        int $notificationLevelMask
    ) {
        $this->notificationLevel = $notificationLevel;
        $this->notificationLevelMask = $notificationLevelMask;
    }
}
