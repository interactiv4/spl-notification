<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\NotificationMessageFactoryInterface;
use Interactiv4\Contracts\SPL\Notification\Api\NotificationMessageInterface;

/**
 * Class NotificationMessageFactory.
 *
 * Common standard for factories that create NotificationMessageInterface objects.
 * Use this class directly or as base class for other implementations.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification
 */
class NotificationMessageFactory implements NotificationMessageFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(
        int $level,
        ?string $title = null,
        ?string $body = null,
        array $additionalData = []
    ): NotificationMessageInterface {
        return new NotificationMessage($level, $title, $body, $additionalData);
    }
}
