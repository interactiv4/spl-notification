<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\NotificationChannelListInterface;
use Interactiv4\Contracts\SPL\Notification\Api\NotificationMessageFactoryInterface;
use Interactiv4\Contracts\SPL\Notification\Api\NotifierCompositeChannelListTrait;
use Interactiv4\Contracts\SPL\Notification\Api\NotifierInterface;

/**
 * Class NotifierCompositeChannelList.
 *
 * Main facade for sending notifications.
 * This implementation uses a channel list to notify a message.
 * Use this class directly or as base class for other implementations.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification
 */
class NotifierCompositeChannelList implements NotifierInterface
{
    use NotifierCompositeChannelListTrait;

    /**
     * NotifierCompositeChannelList constructor.
     *
     * @param NotificationChannelListInterface    $notificationChannelList
     * @param NotificationMessageFactoryInterface $notificationMessageFactory
     */
    public function __construct(
        NotificationChannelListInterface $notificationChannelList,
        NotificationMessageFactoryInterface $notificationMessageFactory
    ) {
        $this->notificationChannelList = $notificationChannelList;
        $this->notificationMessageFactory = $notificationMessageFactory;
    }
}
