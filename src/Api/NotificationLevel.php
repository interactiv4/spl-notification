<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\NotificationLevelInterface;
use Interactiv4\Contracts\SPL\Notification\Api\NotificationLevelTrait;

/**
 * Class NotificationLevel.
 *
 * Notification levels, defined to be used as bitmasks.
 * Use this class directly or as base class for other implementations.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification
 */
class NotificationLevel implements NotificationLevelInterface
{
    use NotificationLevelTrait;
}
