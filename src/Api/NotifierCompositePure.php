<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\NotifierCompositePureTrait;
use Interactiv4\Contracts\SPL\Notification\Api\NotifierInterface;

/**
 * Class NotifierCompositePure.
 *
 * Main facade for sending notifications.
 * This implementation simply delegates to other notifiers.
 * Use this class directly or as base class for other implementations.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification
 */
class NotifierCompositePure implements NotifierInterface
{
    use NotifierCompositePureTrait;

    /**
     * NotifierCompositePure constructor.
     *
     * @param NotifierInterface[] $notifiers
     */
    public function __construct(
        array $notifiers = []
    ) {
        foreach ($notifiers as $notifier) {
            // Enforce strict types
            $this->addNotifier($notifier);
        }
    }
}
