<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Api;

use Interactiv4\Contracts\SPL\Notification\Api\NotificationChannelInterface;
use Interactiv4\Contracts\SPL\Notification\Api\NotificationChannelListInterface;
use Interactiv4\Contracts\SPL\Notification\Api\NotificationChannelListTrait;

/**
 * Class NotificationChannelList.
 *
 * List of notification channels.
 * Use this class directly or as base class for other implementations.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification
 */
class NotificationChannelList implements NotificationChannelListInterface
{
    use NotificationChannelListTrait;

    /**
     * NotificationChannelList constructor.
     *
     * @param NotificationChannelInterface[] $notificationChannels
     */
    public function __construct(
        array $notificationChannels = []
    ) {
        foreach ($notificationChannels as $notificationChannel) {
            // Enforce strict types
            $this->addNotificationChannel($notificationChannel);
        }
    }
}
